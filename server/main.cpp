/*standard include*/
#include <iostream>

/*Qt include*/
#include <QCoreApplication>

/*qxmpp include*/
#include "QXmppLogger.h"
#include "QXmppPasswordChecker.h"
#include "QXmppServer.h"
#include "QXmppClient.h"
#include "QXmppMessage.h"

/*project include*/
#include "slaveClient.hpp"

class passwordChecker : public QXmppPasswordChecker
{
    QXmppPasswordReply::Error getPassword(const QXmppPasswordRequest &request, QString &password)
	{
	    if(request.username() == "master")
	    {
		std::cout<<"MASTER TRY TO CONNECT"<<std::endl;
		password = "masterpassword";
		return QXmppPasswordReply::NoError;
	    }

	    if(request.username() == "slave")
	    {
		std::cout<<"SALVE TRY TO CONNECT"<<std::endl;
		password = "slavepassword";
		return QXmppPasswordReply::NoError;
	    }

	    return QXmppPasswordReply::AuthorizationError;
	}

    bool hasGetPassword() const
	{
	    return true;
	}
};

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    passwordChecker checker;

    QXmppLogger logger;
    logger.setLoggingType(QXmppLogger::StdoutLogging);
    
    QXmppServer server;
    server.setDomain("0.0.0.0");
    server.setLogger(&logger);
    server.setPasswordChecker(&checker);
    server.listenForClients();
    std::cout<<"Start server"<<std::endl;
    
    slaveClient slave;

    slave.logger()->setLoggingType(QXmppLogger::StdoutLogging);
    slave.connectToServer("slave@0.0.0.0", "slavepassword");
    std::cout<<"Start slave"<<std::endl;
    
    return app.exec();
}











