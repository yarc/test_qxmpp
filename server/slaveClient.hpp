#ifndef _SLAVECLIENT_HPP_
#define _SLAVECLIENT_HPP_

#include "QXmppMessage.h"
#include "QXmppClient.h"

class slaveClient : public QXmppClient
{
    Q_OBJECT

public:
    slaveClient(QObject *parent = 0);
    ~slaveClient();
				   
public slots:
    void messageReceive(const QXmppMessage&);
};

#endif

