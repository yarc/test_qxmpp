#include "slaveClient.hpp"
#include <iostream>

slaveClient::slaveClient(QObject *parent) : QXmppClient(parent)
{
    bool check = connect(this, SIGNAL(messageReceived(QXmppMessage)),
			 SLOT(messageReceive(QXmppMessage)));
    Q_ASSERT(check);
    Q_UNUSED(check);
}

slaveClient::~slaveClient()
{

}

void slaveClient::messageReceive(const QXmppMessage& message)
{
    std::cout<<"MESSAGE RECEIVE CALL"<<std::endl;
    if(message.body() == "Test message")
	this->sendPacket(QXmppMessage("", "master", "I recevied your message"));

    std::cout<<"From "<<message.from().toLocal8Bit().constData()<<" : "<<message.body().toLocal8Bit().constData()<<std::endl;
}
