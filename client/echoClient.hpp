#ifndef _ECHOCLIENT_HPP_
#define _ECHOCLIENT_HPP_

#include "QXmppMessage.h"
#include "QXmppClient.h"

class echoClient : public QXmppClient
{
    Q_OBJECT

public:
    echoClient(QObject *parent = 0);
    ~echoClient();
				   
public slots:
    void messageReceived(const QXmppMessage&);
    void afterConnexion();
};

#endif

