/*standard include*/
#include <iostream>

/*Qt include*/
#include <QCoreApplication>

/*QXmpp include*/
#include "QXmppLogger.h"

/*Project include*/
#include "echoClient.hpp"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    echoClient master;

    master.logger()->setLoggingType(QXmppLogger::StdoutLogging);
    master.connectToServer("master@0.0.0.0", "masterpassword");

    master.sendPacket(QXmppMessage("", "salve", "Test message"));
    
    return app.exec();
}


