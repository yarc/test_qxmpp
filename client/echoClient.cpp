#include "echoClient.hpp"
#include <iostream>

echoClient::echoClient(QObject *parent) : QXmppClient(parent)
{
    bool check = connect(this, SIGNAL(messageReceived(QXmppMessage)),
			 SLOT(messageReceived(QXmppMessage)));
    Q_ASSERT(check);
    Q_UNUSED(check);

    check = connect(this, SIGNAL(connected()), SLOT(afterConnexion()));

    Q_ASSERT(check);
    Q_UNUSED(check);
}

echoClient::~echoClient()
{

}

void echoClient::messageReceived(const QXmppMessage& message)
{
    QString from = message.from();
    QString msg = message.body();

	    
    std::cout<<"From "<<from.toLocal8Bit().constData()<<" : "<<msg.toLocal8Bit().constData()<<std::endl;
    this->sendPacket(QXmppMessage("", from, "Your message: " + msg));
}

void echoClient::afterConnexion()
{
    std::cout<<"AFTER CONNEXION SEND TEST MESSAGE"<<std::endl;
    this->sendPacket(QXmppMessage("", "salve", "Test message"));
}









